#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# set keyboard layout to bepo
alias jkl='setxkbmap.exe -layout fr -variant bepo -model pc'
